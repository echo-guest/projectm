#!/bin/sh
#
#  Script to make an existing projectM source tarball DFSG-compliant
#
#  Copyright (C) 2010 Matthias Klumpp
#   based on script by Reinhard Tartler
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License along
#  with this program; if not, write to the Free Software Foundation, Inc.,
#  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

set -eu

usage() {
	cat >&2 <<EOF
usage: $0 [-dh]
  -h : display help
  -t : original upstream tarball
  -o : output tarball name
  -v : upstream version
EOF
}

debug () {
	$DEBUG && echo "DEBUG: $*" >&2
}

error () {
	echo "$1" >&2
	exit 1;
}

set +e
PARAMS=`getopt ht:v: "$@"`
if test $? -ne 0; then usage; exit 1; fi;
set -e

eval set -- "$PARAMS"

DEBUG=false
USVERSION=2.1.0
TARBALL=""

while test $# -gt 0
do
	case $1 in
		-h) usage; exit 1 ;;
		-t) ORIGTAR=$2; shift ;;
		-v) USVERSION=$2; shift ;;
		-o) TARBALL=$2; shift ;;
		--) shift ; break ;;
		*)  echo "Internal error!" ; exit 1 ;;
	esac
	shift
done

# sanity checks now
dh_testdir

if [ -z $ORIGTAR ]; then
	error "you need to specify the original upstream tarball!"
fi

PACKAGENAME=projectm
if [ "$TARBALL" = "" ]; then
	TARBALL="./${PACKAGENAME}_${USVERSION}+dfsg.orig.tar.gz"
fi

TMPDIR=`mktemp -d`
trap 'rm -rf ${TMPDIR}'  EXIT

mkdir ${TMPDIR}/${PACKAGENAME}
ODIR=`pwd`
cd ${TMPDIR}/${PACKAGENAME}

tar xzf ${ORIGTAR}
cd projectM-complete-${USVERSION}-Source

rm -rf ./src/WinLibs
rm -rf ./src/macos
rm -rf ./src/win32
rm -f ./INSTALL-iTunes-macos.txt
rm -rf ./playlists
rm -rf ./src/projectM-iTunes-VizKit
rm -rf ./src/projectM-iTunes
rm -rf ./src/projectM-moviegen
rm -rf ./src/projectM-screensaver
rm -rf ./src/projectM-wmp
find . -type d -name CVS -exec rm -rf {} +
find . -type d -name *~ -exec rm {} +

cd ${ODIR}
tar czf ${TARBALL} -C ${TMPDIR}/${PACKAGENAME} projectM-complete-${USVERSION}-Source
